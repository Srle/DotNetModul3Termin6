﻿using Radnici.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Radnici.Controllers
{
    public class ZaposleniController : ApiController
    {
        Zaposleni[] svizaposleni = new Zaposleni[]
        {
            new Zaposleni(1, "Pera", "Peric", 31), new Zaposleni(2, "Mika", "Savic", 25), new Zaposleni(3, "Zika", "Stanic", 42), new Zaposleni(4, "Milica", "Acic", 40)
        };

        // GET /api/zaposleni
        public IHttpActionResult Get()
        {
            return Ok(svizaposleni);
        }

        // GET /api/zaposleni?id=1
        public IHttpActionResult Get(int id)
        {
            foreach (Zaposleni zap in svizaposleni)
            {
                if (zap.Id ==id)
                {
                    return Ok(zap);
                }
            }

            return NotFound();
        }


        // GET /api/zaposleni?ime="Pera"
        public IHttpActionResult Get(string ime)
        {
            foreach (Zaposleni zap in svizaposleni)
            {
                if (zap.Ime.Equals(ime))
                {
                    return Ok(zap);
                }
            }

            return NotFound();
        }

        // POST /api/zaposleni sa JSON objektom
        public Zaposleni Post(Zaposleni zap)
        {
            return zap;
        }

        // PUT api/zaposleni/1
        public Zaposleni Put(int id, Zaposleni zap)
        {
            return zap;

        }

        // DELETE api/zaposleni/1
        public void Delete(int id)
        {

        }

    }
}
