﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Radnici.Models
{
    public class Zaposleni
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Godine { get; set; }

        public Zaposleni(int id, string ime, string prezime, int godine)
        {
            this.Id = id;
            this.Ime = ime;
            this.Prezime = prezime;
            this.Godine = godine;
        }

        public Zaposleni()
        {
        }

    }
}