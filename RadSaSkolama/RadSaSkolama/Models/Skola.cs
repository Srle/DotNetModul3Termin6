﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RadSaSkolama.Models
{
    public class Skola
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Mesto { get; set; }
        public int GodinaPocetka { get; set; }

        public Skola(int id, string ime, string mesto, int godina)
        {
            this.Id = id;
            this.Ime = ime;
            this.Mesto = mesto;
            this.GodinaPocetka = godina;
        }

        public Skola()
        {
        }




    }
}