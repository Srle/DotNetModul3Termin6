﻿using RadSaSkolama.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RadSaSkolama.Controllers
{
    public class SkolaController : ApiController
    {
        Skola[] sveskole = new Skola[]
        {
            new Skola(1, "Vuk Karadzic", "Beograd", 1952), new Skola(2, "Mika Antic", "Smederevo", 1963), new Skola(3, "Desanka Maksimovic", "Jagodina", 1830), new Skola(4, "Sveti Sava", "Novi Sad", 1970)
        };

        // GET /api/skola
        public IHttpActionResult Get()
        {
            return Ok(sveskole);
        }

        // GET /api/skola?id=1
        public IHttpActionResult Get(int id)
        {
            foreach (Skola skl in sveskole)
            {
                if (skl.Id == id)
                {
                    return Ok(skl);
                }
            }

            return NotFound();
        }


        // GET /api/skola?godina=1
        public IHttpActionResult Getgodina(int godina)
        {
            List<Skola> lista = new List<Skola>();
            foreach (Skola skl in sveskole)
            {
                if (skl.GodinaPocetka < godina)
                {
                    lista.Add(skl);
                }
            }
            Skola[] nekeskole = new Skola[lista.Count];

            for (int i = 0; i < lista.Count; i++)
            {
                nekeskole[i] = lista[i];
            }

            return Ok(nekeskole);
        }

        // POST /api/skola sa JSON objektom
        public Skola Post(Skola skl)
        {
            return skl;
        }

        // PUT api/skola/1
        public Skola Put(int id, Skola skl)
        {
            return skl;

        }

        // DELETE api/skola/1
        public void Delete(int id)
        {

        }

    }
}
